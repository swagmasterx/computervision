#include "cv.common.h"

#include <array>
#include <limits>
#include <algorithm>

CV::Vector CV::Kernel::derativeX()
{
    Vector res{{3, 3}};
    res.set({0, 0}, 1);
    res.set({0, 1}, 0);
    res.set({0, 2}, -1);
    res.set({1, 0}, 2);
    res.set({1, 1}, 0);
    res.set({1, 2}, -2);
    res.set({2, 0}, 1);
    res.set({2, 1}, 0);
    res.set({2, 2}, -1);
    return res;
}

CV::Vector CV::Kernel::derativeY()
{
    Vector res{{3, 3}};
    res.set({0, 0}, 1);
    res.set({0, 1}, 2);
    res.set({0, 2}, 1);
    res.set({1, 0}, 0);
    res.set({1, 1}, 0);
    res.set({1, 2}, 0);
    res.set({2, 0}, -1);
    res.set({2, 1}, -2);
    res.set({2, 2}, -1);
    return res;
}

CV::Vector CV::Kernel::GaussianX(double sigma)
{
    Vector result{{(static_cast<int>(6 * sigma) % 2) ? static_cast<int>(6 * sigma) : (static_cast<int>(6 * sigma) + 1), 1}};
    for (int j = 0; j < result.size().width(); j++) {
        result.set({0, j}, Utils::gaussianFunction(j - result.size().width() / 2, sigma));
    }
    return result;
}

CV::Vector CV::Kernel::GaussianY(double sigma)
{
    Vector result{{1, (static_cast<int>(6 * sigma) % 2) ? static_cast<int>(6 * sigma) : (static_cast<int>(6 * sigma) + 1)}};
    for (int i = 0; i < result.size().height(); i++) {
        result.set({i, 0}, Utils::gaussianFunction(i - result.size().height() / 2, sigma));
    }
    return result;
}

CV::Vector CV::Kernel::Gaussian(int halfSize)
{
    Vector result{{halfSize * 2 + 1, halfSize * 2 + 1}};
    const auto sigma = static_cast<double>(halfSize) / 3;
    for (auto i {0}; i < result.size().height(); i++) {
        for (auto j {0}; j < result.size().width(); j++) {
            result.set({i, j}, Utils::gaussianFunction
                    (i - result.size().height() / 2, j - result.size().width() / 2, sigma));
        }
    }
    return result;
}

CV::Vector CV::Kernel::Gaussian(double sigma)
{
    const auto size {(static_cast<int>(6 * sigma) % 2) ? static_cast<int>(6 * sigma) : (static_cast<int>(6 * sigma) + 1)};
    Vector result{{size, size}};
    for (auto i {0}; i < result.size().height(); i++) {
        for (auto j {0}; j < result.size().width(); j++) {
            result.set({i, j}, Utils::gaussianFunction
                    (i - result.size().height() / 2, j - result.size().width() / 2, sigma));
        }
    }
    return result;
}

CV::Vector CV::Kernel::Gaussian(int halfSize, double sigma)
{
    Vector result{{halfSize * 2 + 1, halfSize * 2 + 1}};
    for (auto i {0}; i < result.size().height(); i++) {
        for (auto j {0}; j < result.size().width(); j++) {
            result.set({i, j}, Utils::gaussianFunction
                    (i - result.size().height() / 2, j - result.size().width() / 2, sigma));
        }
    }
    return result;
}

double CV::Utils::gaussianFunction(int y, int x, double sigma)
{
    return 1 / (2 * kPi * (sigma * sigma)) * std::exp(-((y * y) + (x * x)) / (2 * (sigma * sigma)));
}

double CV::Utils::gaussianFunction(int a, double sigma)
{
    return 1 / (std::sqrt(2 * kPi) * sigma) * std::exp(-(a * a) / (2 * (sigma * sigma)));
}

double CV::Utils::sigma(double otherSigma, double resultSigma)
{
    return std::sqrt(resultSigma * resultSigma - otherSigma * otherSigma);
}

CV::Vector CV::applyKernel(const CV::Vector &source, const CV::Vector &kernel, CV::EdgeMethod edgeMethod)
{
    if (kernel.size().width() % 2 != 1 || kernel.size().height() % 2 != 1) throw std::invalid_argument{"both dimensions of the kernel must be odd"};

    Vector res{source.size()};
    for (auto i {0}; i < res.size().height(); i++) {
        for (auto j {0}; j < res.size().width(); j++) {
            const auto centerY {kernel.size().height() / 2};
            const auto centerX {kernel.size().width() / 2};
            auto sumY {0.};
            for (auto u {-centerY}; u <= centerY; u++) {
                auto sumX {0.};
                for (auto v {-centerX}; v <= centerX; v++) {
                    const auto value {source.at({i + u, j + v}, edgeMethod)};
                    sumX += value * kernel.at({centerY + u, centerX + v});
                }
                sumY += sumX;
            }
            res.set({i, j}, sumY);
        }
    }
    return res;
}

CV::Vector CV::applyGaussian(const CV::Vector &source, double sigma)
{
    return applyKernel(source, Kernel::Gaussian(sigma));
}

CV::Vector CV::gradMagnitude(const CV::Vector &source)
{
    const auto derativeX {applyKernel(source, Kernel::derativeX())};
    const auto derativeY {applyKernel(source, Kernel::derativeY())};
    return pow(pow(derativeX, 2) + pow(derativeY, 2), 0.5);
}

CV::Vector CV::gradDirection(const CV::Vector &source)
{
    const auto derativeX {applyKernel(source, Kernel::derativeX())};
    const auto derativeY {applyKernel(source, Kernel::derativeY())};
    return atan2(derativeY, derativeX);
}

std::vector<CV::IPoint> CV::harris(const CV::Vector &source, int p, double t, int windowSize)
{
    if (windowSize % 2 != 1) throw std::invalid_argument{"window size must be odd"};
    Vector harrisValues{source.size()};

    const auto derativeX {applyKernel(source, Kernel::derativeX())};
    const auto derativeY {applyKernel(source, Kernel::derativeY())};

    const auto coefficient {Kernel::Gaussian(windowSize / 2)};

    for (auto i {0}; i < harrisValues.size().height(); i++) {
        for (auto j {0}; j < harrisValues.size().width(); j++) {
            const auto center {windowSize / 2};
            std::array<std::array<double, 2>, 2> h {{{0.}}};
            for (auto u {-center}; u <= center; u++) {
                for (auto v {-center}; v <= center; v++) {
                    const auto iX {derativeX.at({i + u, j + v}, CV::EdgeMethod::Reflect)};
                    const auto iY {derativeY.at({i + u, j + v}, CV::EdgeMethod::Reflect)};
                    const auto distanceCoefficient = coefficient.at({center + u, center + v});
                    h[0][0] += distanceCoefficient * iX * iX;
                    h[0][1] += distanceCoefficient * iX * iY;
                    h[1][0] += distanceCoefficient * iX * iY;
                    h[1][1] += distanceCoefficient * iY * iY;
                }
            }

            const auto det {h[0][0] * h[1][1] - h[0][1] * h[1][0]};
            const auto trace {h[0][0] + h[1][1]};
            const auto f {det / trace};

            if (f > t) {
                harrisValues.set({i, j}, f);
            } else {
                harrisValues.set({i, j}, std::numeric_limits<double>().min());
            }
        }
    }

    return nms(harrisValues, p);
}

std::vector<CV::IPoint> CV::nms(const CV::Vector &detectorValues, int p)
{
    std::vector<IPoint> ipoints;
    for (auto i {0}; i < detectorValues.size().height(); i++) {
        for (auto j {0}; j < detectorValues.size().width(); j++) {
            if (detectorValues.at({i, j})
                    != std::numeric_limits<double>().min()) {
                const auto center {p};
                auto isMaximum {true};
                for (auto u {-center}; u <= center && isMaximum; u++) {
                    for (auto v {-center}; v <= center && isMaximum; v++) {
                        const auto val {detectorValues.at({i + u, j + v})};
                        if (val > detectorValues.at({i, j})) {
                            isMaximum = false;
                        }
                    }
                }
                if (isMaximum) {
                    ipoints.push_back({{i, j}, detectorValues.at({i, j})});
                }
            }
        }
    }
    return ipoints;
}

void CV::anms(std::vector<CV::IPoint> &ipoints, int neededAmount)
{
    std::vector<IPoint> tmpVector;
    tmpVector.reserve(ipoints.size());
    auto r {1};
    while (ipoints.size() > neededAmount) {
        for (const auto &curPoint : ipoints) {
            auto isMaximum {true};
            for (auto i {0}; i < ipoints.size() && isMaximum; i++) {
                const auto varPoint {ipoints.at(i)};
                const auto distance {d1(varPoint.coord, curPoint.coord)};
                if (distance <= r) {
                    if (varPoint.detectorVal * 0.9 > curPoint.detectorVal) {
                        isMaximum = false;
                    }
                }
            }
            if (isMaximum) {
                tmpVector.push_back(curPoint);
            }
        }
        ipoints = tmpVector;
        tmpVector.clear();
        r++;
    }
}

void CV::sift(std::vector<CV::IPoint> &ipoints, const CV::Vector &source)
{
    const auto histogramCount {16};
    const auto binsCount {8};
    const auto radius {8};
    const auto step {2 * Utils::kPi / binsCount};
    const auto siftAddToBin {
        [&histogramCount](int i, int j, int bin, double val, Vector &descriptor) {
            const auto sideSize {static_cast<int>(std::sqrt(histogramCount))};
            const auto bi {i - i % sideSize};
            const auto bj {j - j % sideSize};
            const auto index {bi * sideSize + bj + bin};

            descriptor.directSet(index, descriptor.directAt(index) + val);
        }
    };

    const auto gradMag {gradMagnitude(source)};
    const auto gradDir {gradDirection(source)};
    const auto coefficient {Kernel::Gaussian(radius)};

    for (auto &ipoint : ipoints) {
        ipoint.descriptor = Vector{{1, binsCount * histogramCount}};

        for (auto i {0}; i < radius * 2; i++) {
            for (auto j {0}; j < radius * 2; j++) {
                const auto curDir = gradDir.at({i - radius + ipoint.coord.y, j - radius + ipoint.coord.x}) + Utils::kPi;
                const auto curMag = gradMag.at({i - radius + ipoint.coord.y, j - radius + ipoint.coord.x}) * coefficient.at({i, j});
                const auto b1 {static_cast<int>(curDir / step)};
                const auto b2 {[=]() {
                    const auto b2 {curDir > b1 * step + step / 2 ? b1 + 1 : b1 - 1};
                    if (b2 == binsCount) {
                        return 0;
                    }
                    if (b2 == -1) {
                        return binsCount - 1;
                    }
                    return b2;
                }()};
                const auto b1Center {b1 * step + step / 2};

                const auto b1Val {(1 - (std::abs(curDir - b1Center) / step)) * curMag};
                const auto b2Val {curMag - b1Val};

                siftAddToBin(i, j, b1, b1Val, ipoint.descriptor);
                siftAddToBin(i, j, b2, b2Val, ipoint.descriptor);
            }
        };
        ipoint.descriptor.normalize();
        ipoint.descriptor.clamp(0.2);
        ipoint.descriptor.normalize();
    }
}

std::vector<CV::IPoint>::const_iterator CV::mapIPoint(const CV::IPoint &givenIPoint, const std::vector<CV::IPoint> &otherIPoints)
{
    auto minDis {std::numeric_limits<double>::max()};
    auto min {otherIPoints.begin()};
    for (auto it {otherIPoints.begin()}; it != otherIPoints.end(); it++) {
        const auto newDis {d1(givenIPoint.descriptor, (*it).descriptor)};
        if (newDis < minDis) {
            minDis = newDis;
            min = it;
        }
    }
    return min;
}

CV::Vector CV::Utils::glueTogether(const CV::Vector &v1, const CV::Vector &v2)
{
    Vector res{{std::max(v1.size().height(), v2.size().height()), v1.size().width() + v2.size().width()}};
    for (auto i {0}; i < res.size().height(); i++) {
        for (auto j {0}; j < res.size().width(); j++) {
            if (j < v1.size().width()) {
                if (i < v1.size().height()) {
                    res.set({i, j}, v1.at({i, j}));
                } else {
                    res.set({i, j}, 0.);
                }
            } else {
                if (i < v2.size().height()) {
                    res.set({i, j}, v2.at({i, j - v1.size().width()}));
                } else {
                    res.set({i, j}, 0.);
                }
            }
        }
    }
    return res;
}

void CV::siftInvRotation(std::vector<CV::IPoint> &ipoints, const CV::Vector &source)
{
    const auto numberOfHistagrams {16};
    const auto numberOfBins {8};
    const auto gridHalfSize {8};
    const auto step {2 * Utils::kPi / numberOfBins};
    const auto siftAddToBin {
        [&numberOfHistagrams](int i, int j, int bin, double val, Vector &descriptor) {
            const auto sideSize {static_cast<int>(std::sqrt(numberOfHistagrams))};
            const auto bi {i - i % sideSize};
            const auto bj {j - j % sideSize};
            const auto index {bi * sideSize + bj + bin};

            descriptor.directSet(index, descriptor.directAt(index) + val);
        }
    };

    const auto gradMag {gradMagnitude(source)};
    const auto gradDir {gradDirection(source)};

    std::vector<CV::IPoint> newIPoints;
    for (auto &ipoint : ipoints) {
        Vector descriptor{{1, numberOfBins * numberOfHistagrams}};
        const auto peaks {orientationPeaks(gradMag, gradDir, ipoint.coord)};

        auto first {true};
        for (const auto peakDir : peaks) {
            for (auto i {0}; i < gridHalfSize * 2; i++) {
                for (auto j {0}; j < gridHalfSize * 2; j++) {
                    auto curDir = gradDir.at({i - gridHalfSize + ipoint.coord.y, j - gridHalfSize + ipoint.coord.x}) + 3 * Utils::kPi - peakDir;
                    const auto curMag = gradMag.at({i - gridHalfSize + ipoint.coord.y, j - gridHalfSize + ipoint.coord.x});
                    curDir = std::fmod(curDir, 2 * Utils::kPi);
                    const auto b1 {static_cast<int>(curDir / step)};
                    const auto b2 {[=]() {
                        const auto b2 {curDir > b1 * step + step / 2 ? b1 + 1 : b1 - 1};
                        if (b2 == numberOfBins) {
                            return 0;
                        }
                        if (b2 == -1) {
                            return numberOfBins - 1;
                        }
                        return b2;
                    }()};
                    const auto b1Center {b1 * step + step / 2};

                    const auto b1Val {(1 - (std::abs(curDir - b1Center) / step)) * curMag};
                    const auto b2Val {curMag - b1Val};

                    const auto iR {round((i - gridHalfSize) * std::cos(peakDir) + (j - gridHalfSize) * std::sin(peakDir))};
                    const auto jR {round(-(i - gridHalfSize) * std::sin(peakDir) + (j - gridHalfSize) * std::cos(peakDir))};

                    if (iR < -gridHalfSize || iR >= gridHalfSize || jR < -gridHalfSize || jR >= gridHalfSize) {
                        continue;
                    }

                    siftAddToBin(iR + gridHalfSize, jR + gridHalfSize, b1, b1Val, descriptor);
                    siftAddToBin(iR + gridHalfSize, jR + gridHalfSize, b2, b2Val, descriptor);
                }
            };
            descriptor.normalize();
            descriptor.clamp(0.2);
            descriptor.normalize();

            if (first) {
                ipoint.descriptor = descriptor;
                first = false;
            } else {
                newIPoints.push_back({ipoint.coord, ipoint.detectorVal, descriptor});
            }
        }
    }
    ipoints.insert(ipoints.end(), newIPoints.begin(), newIPoints.end());
}

std::vector<double> CV::orientationPeaks(const CV::Vector &gradMag, const CV::Vector &gradDir, const CV::Point &point)
{
    constexpr auto numberOfBins {36};
    const auto gridHalfSize {8};
    const auto step {2 * Utils::kPi / numberOfBins};
    const auto findTwoMax {
        [&numberOfBins](const std::array<double, numberOfBins> &bins){
            auto maxI1 {0};
            auto maxI2 {0};
            auto max1 {bins[maxI1]};
            auto max2 {bins[maxI2]};
            for (auto i {0}; i < bins.size(); i++) {
                if ((bins[i] > bins[(i - 1 + bins.size()) % bins.size()]) && (bins[i] > bins[(i + 1) % bins.size()])) {
                    if (bins[i] > max1) {
                        maxI2 = maxI1;
                        max2 = bins[maxI2];
                        maxI1 = i;
                        max1 = bins[maxI1];
                    } else if (bins[i] > max2) {
                        maxI2 = i;
                        max2 = bins[maxI2];
                    }
                }
            }
            return std::make_pair(maxI1, maxI2);
        }
    };
    const auto interpolate {
        [&numberOfBins](int index, const std::array<double, numberOfBins> &bins) {
            const auto lVal {bins[(index - 1 + bins.size()) % bins.size()]};
            const auto rVal {bins[(index + 1) % bins.size()]};
            const auto val {bins[index]};

            const auto step {2 * Utils::kPi / bins.size()};
            const auto interpolated {(lVal - rVal) / (2 * (lVal + rVal - 2 * val))};
            return (interpolated + index) * step + (step / 2);
        }
    };

    std::array<double, numberOfBins> bins {{0.}};
    const auto coefficient {Kernel::Gaussian(gridHalfSize, 40)};

    for (auto i {0}; i < gridHalfSize * 2; i++) {
        for (auto j {0}; j < gridHalfSize * 2; j++) {
            const auto curDir = gradDir.at({i - gridHalfSize + point.y, j - gridHalfSize + point.x}) + Utils::kPi;
            const auto curMag = gradMag.at({i - gridHalfSize + point.y, j - gridHalfSize + point.x}) * coefficient.at({i, j});
            const auto b1 {static_cast<int>(curDir / step)};
            const auto b2 {[=]() {
                const auto b2 {curDir > b1 * step + step / 2 ? b1 + 1 : b1 - 1};
                if (b2 == numberOfBins) {
                    return 0;
                }
                if (b2 == -1) {
                    return numberOfBins - 1;
                }
                return b2;
            }()};
            const auto b1Center {b1 * step + step / 2};

            const auto b1Val {(1 - (std::abs(curDir - b1Center) / step)) * curMag};
            const auto b2Val {curMag - b1Val};

            bins[b1] += b1Val;
            bins[b2] += b2Val;
        }
    };

    const auto maxs {findTwoMax(bins)};

    std::vector<double> peaks;
    peaks.push_back(interpolate(maxs.first, bins));
    if (maxs.second != maxs.first && bins[maxs.second] >= bins[maxs.first] * 0.8) {
        peaks.push_back(interpolate(maxs.second, bins));
    }
    return peaks;
}
