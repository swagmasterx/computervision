#ifndef CV_COMMON_H
#define CV_COMMON_H

#include "structures/cv.vector.h"
#include <cmath>

namespace CV {
    namespace Utils {
        const auto kPi = std::acos(-1);

        double gaussianFunction(int y, int x, double sigma);
        double gaussianFunction(int a, double sigma);
        double sigma(double otherSigma, double resultSigma);
        Vector glueTogether(const Vector &v1, const Vector &v2);
    }

    namespace Kernel {
        Vector derativeX();
        Vector derativeY();

        Vector GaussianX(double sigma);
        Vector GaussianY(double sigma);
        Vector Gaussian(int halfSize);
        Vector Gaussian(double sigma);
        Vector Gaussian(int halfSize, double sigma);
    }

    struct IPoint {
        Point coord;
        double detectorVal {0.};
        Vector descriptor;
    };

    Vector applyKernel(const Vector &source, const Vector &kernel,
                                   CV::EdgeMethod edgeMethod = CV::EdgeMethod::CopyEdge);
    Vector applyGaussian(const Vector &source, double sigma);
    Vector gradMagnitude(const Vector &source);
    Vector gradDirection(const Vector &source);

    std::vector<IPoint> harris(const Vector &source, int p, double t, int windowSize = 5);
    std::vector<IPoint> nms(const Vector &detectorValues, int p);
    void anms(std::vector<IPoint> &ipoints, int neededAmount);

    void sift(std::vector<IPoint> &ipoints, const Vector &source);
    void siftInvRotation(std::vector<IPoint> &ipoints, const Vector &source);
    std::vector<double> orientationPeaks(const Vector &gradMag, const Vector &gradDir, const Point &point);

    std::vector<IPoint>::const_iterator mapIPoint(const IPoint &givenIPoint, const std::vector<IPoint> &otherIPoints);
}

#endif // CV_COMMON_H
