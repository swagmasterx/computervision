#include "cv.converter.h"

#include <exception>

CV::Vector CV::Converter::qImageToVector(const QImage &qimage)
{
    if (qimage.isNull()) throw std::invalid_argument{"qimage is null"};

    Vector res{{qimage.height(), qimage.width()}};
    for (auto i {0}; i < res.size().height(); i++) {
        for (auto j {0}; j < res.size().width(); j++) {
            res.set({i, j}, qColorToGreyscale(qimage.pixelColor(j, i)));
        }
    }
    return res;
}

double CV::Converter::qColorToGreyscale(const QColor &qcolor)
{
    //return 0.2126 * qcolor.red() + 0.7152 * qcolor.green() + 0.0722 * qcolor.blue();
    return 0.213 * qcolor.red() + 0.715 * qcolor.green() + 0.072 * qcolor.blue();
}

QImage CV::Converter::vectorToQImage(const CV::Vector &vector)
{
    QImage qimage(vector.size().width(), vector.size().height(), QImage::Format_RGB32);
    for (auto i{0}; i < qimage.height(); i++) {
        for (auto j {0}; j < qimage.width(); j++) {
            const auto greyscale {static_cast<int>(vector.at({i, j}))};
            qimage.setPixelColor(j, i, QColor(greyscale, greyscale, greyscale));
        }
    }
    return qimage;
}
