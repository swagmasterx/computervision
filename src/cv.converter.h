#ifndef CV_CONVERTER_H
#define CV_CONVERTER_H

#include <QImage>

#include "src/structures/cv.vector.h"

namespace CV {
namespace Converter {
    Vector qImageToVector(const QImage &qimage);
    QImage vectorToQImage(const Vector &vector);
    double qColorToGreyscale(const QColor &qcolor);
}
}

#endif // CV_CONVERTER_H
