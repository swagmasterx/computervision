#include "imageprocessor.h"

#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QtMath>
#include <QDebug>
#include <QSet>

ImageProcessor::ImageProcessor()
{

}

Image ImageProcessor::applyKernel(const Image &image, const Kernel &kernel,
                                        Image::EdgeMethod edgeMethod)
{
    Image newImage(image.width(), image.height());
    for (int i = 0; i < image.height(); i++) {
        for (int j = 0; j < image.width(); j++) {
            int centerY = kernel.centerY();
            int centerX = kernel.centerX();
            double sumY = 0;
            for (int u = -centerY; u <= centerY; u++) {
                double sumX = 0;
                for (int v = -centerX; v <= centerX; v++) {
                    double value = image.at(i + u, j + v, edgeMethod);
                    sumX += value * kernel.at(centerY + u, centerX + v);
                }
                sumY += sumX;
            }
            double newValue = sumY;
            newImage.set(i, j, newValue);
        }
    }
    return newImage;
}

Image ImageProcessor::applyGaussian(const Image &image, double sigma)
{
    return applyKernel(applyKernel(image, Kernel::GaussianY(sigma)), Kernel::GaussianX(sigma));
}

Image ImageProcessor::sobelOperator(const Image &image)
{
    Image derativeX = applyKernel(applyKernel(image, Kernel::derativeXY()), Kernel::derativeXX());
    Image derativeY = applyKernel(applyKernel(image, Kernel::derativeYY()), Kernel::derativeYX());
    return pow(pow(derativeX, 2) + pow(derativeY, 2), 0.5);
}

QPair<Image, Image> ImageProcessor::gradientValDir(const Image &image)
{
    Image derativeX = applyKernel(applyKernel(image, Kernel::derativeXY()), Kernel::derativeXX());
    Image derativeY = applyKernel(applyKernel(image, Kernel::derativeYY()), Kernel::derativeYX());
    return qMakePair(pow(pow(derativeX, 2) + pow(derativeY, 2), 0.5), atan2(derativeY, derativeX));
}

QVector<IPoint> ImageProcessor::moravec(const Image &image, int p, int t, int windowSize)
{
    Q_ASSERT(windowSize % 2 == 1);
    Image moravecValues(image.width(), image.height());
    QVector<double> shiftValues(8);
    int shiftValuesIndex;
    for (int i = 0; i < image.height(); i++) {
        for (int j = 0; j < image.width(); j++) {
            shiftValuesIndex = 0;
            for (int k = -1; k <= 1; k++) {
                for (int l = -1; l <= 1; l++) {
                    if (k == 0 && l == 0) {
                        continue;
                    }

                    int center = windowSize / 2;
                    double sumY = 0;
                    for (int u = -center; u <= center; u++) {
                        double sumX = 0;
                        for (int v = -center; v <= center; v++) {
                            double baseValue = image.at(i + u, j + v, Image::EdgeMethod::Black);
                            double shiftValue = image.at(i + u + k,
                                                              j + v + l, Image::EdgeMethod::Black);
                            sumX += qPow(baseValue - shiftValue, 2);
                        }
                        sumY += sumX;
                    }
                    shiftValues[shiftValuesIndex++] = sumY;
                }
            }

            double min = shiftValues.at(0);
            for (int k = 0; k < shiftValues.size(); k++) {
                if (shiftValues.at(k) < min) {
                    min = shiftValues.at(k);
                }
            }

            if (min > t) {
                moravecValues.set(i, j, min);
            } else {
                moravecValues.set(i, j, std::numeric_limits<double>().min());
            }
        }
    }

    return nms(moravecValues, p);
}

QVector<IPoint> ImageProcessor::harris(const Image &image, int p, int t, int windowSize)
{
    Q_ASSERT(windowSize % 2 == 1);
    Image harrisValues(image.width(), image.height());

    Image derativeX = applyKernel(applyKernel(image, Kernel::derativeXY()), Kernel::derativeXX());
    Image derativeY = applyKernel(applyKernel(image, Kernel::derativeYY()), Kernel::derativeYX());

    Kernel coefficient = Kernel::Gaussian(windowSize / 2);

    for (int i = 0; i < image.height(); i++) {
        for (int j = 0; j < image.width(); j++) {
            int center = windowSize / 2;
            double h[2][2] = {{ 0 }};
            for (int u = -center; u <= center; u++) {
                double hX[2][2] {{ 0 }};
                for (int v = -center; v <= center; v++) {
                    double iX = derativeX.at(i + u, j + v, Image::EdgeMethod::Black);
                    double iY = derativeY.at(i + u, j + v, Image::EdgeMethod::Black);
                    double distanceCoefficient = coefficient.at(center + u, center + v);
                    hX[0][0] += distanceCoefficient * iX * iX;
                    hX[0][1] += distanceCoefficient * iX * iY;
                    hX[1][0] += distanceCoefficient * iX * iY;
                    hX[1][1] += distanceCoefficient * iY * iY;
                }
                h[0][0] += hX[0][0];
                h[0][1] += hX[0][1];
                h[1][0] += hX[1][0];
                h[1][1] += hX[1][1];
            }

            double det = h[0][0] * h[1][1] - h[0][1] * h[1][0];
            double trace = h[0][0] + h[1][1];
            double f = det / trace;

            if (f > t) {
                harrisValues.set(i, j, f);
            } else {
                harrisValues.set(i, j, std::numeric_limits<double>().min());
            }
        }
    }

    return nms(harrisValues, p);
}

QVector<IPoint> ImageProcessor::anms(const QVector<IPoint> &ipoints, int neededAmount)
{
    QVector<IPoint> curVector = ipoints;
    QVector<IPoint> tmpVector;
    tmpVector.reserve(ipoints.size());
    int r = 1;
    while (curVector.size() > neededAmount) {
        for (const IPoint &curPoint : curVector) {
            bool isMaximum = true;
            for (int i = 0; i < curVector.size() && isMaximum; i++) {
                IPoint varPoint = curVector.at(i);
                double distance = qSqrt((varPoint.first.x() - curPoint.first.x())
                                        * (varPoint.first.x() - curPoint.first.x())
                                        + (varPoint.first.y() - curPoint.first.y())
                                        * (varPoint.first.y() - curPoint.first.y()));
                if (distance <= r) {
                    if (varPoint.second * 0.9 > curPoint.second) {
                        isMaximum = false;
                    }
                }
            }
            if (isMaximum) {
                tmpVector.append(curPoint);
            }
        }
        curVector = tmpVector;
        tmpVector.clear();
        r++;
    }
    return curVector;
}

QVector<IPoint> ImageProcessor::nms(const Image &image, int p)
{
    QVector<IPoint> redDots;
    for (int i = 0; i < image.height(); i++) {
        for (int j = 0; j < image.width(); j++) {
            if (image.at(i, j)
                    != std::numeric_limits<double>().min()) {
                int center = p;
                bool isMaximum = true;
                for (int u = -center; u <= center && isMaximum; u++) {
                    for (int v = -center; v <= center && isMaximum; v++) {
                        double val = 0;
                        if (!(i + u >= image.height() || i + u < 0
                              || j + v >= image.width() || j + v < 0)) {
                            val = image.at(i + u, j + v);
                        }
                        if (val > image.at(i, j)) {
                            isMaximum = false;
                        }
                    }
                }
                if (isMaximum) {
                    redDots.append(IPoint(QPoint(j, i), image.at(i, j)));
                }
            }
        }
    }
    return redDots;
}

QVector<QPair<QPoint, Image>> ImageProcessor::descriptors(const QVector<IPoint> &ipoints, const Image &image)
{
    QVector<QPair<QPoint, Image>> res(ipoints.size());
    for (auto i{0}; i < res.size(); i++) {
        res[i].first = ipoints.at(i).first;
        res[i].second = descriptorForPoint(ipoints.at(i).first, image);
    }
    return res;
}

QPair<QPoint, Image> ImageProcessor::closestDescriptor(const QPair<QPoint, Image> &source, const QVector<QPair<QPoint, Image> > &others)
{
    auto minDis {std::numeric_limits<double>::max()};
    auto min {others.at(0)};
    for (const auto &other : others) {
        const auto newDis = distance(source.second, other.second);
        if (newDis < minDis) {
            minDis = newDis;
            min = other;
        }
    }
    return min;
}

QVector<QPair<QPoint, Image>> ImageProcessor::closestDescriptor(const QPair<QPoint, Image> &source, QVector<QPair<QPoint, Image> > others, int n)
{
    QVector<QPair<QPoint, Image>> res;
    QVector<QPoint> seen;
    auto prevDis {0.};
    for (auto i {0}; i < n; i++) {
        auto notFound {true};
        while (notFound) {
            auto minDis {std::numeric_limits<double>::max()};
            auto min {others.at(0)};
            for (const auto &other : others) {
                if (seen.contains(other.first)) {
                    continue;
                }

                const auto newDis = distance(source.second, other.second);
                if (newDis < minDis) {
                    minDis = newDis;
                    min = other;
                }
            }
            if (prevDis / minDis < 0.8) {
                prevDis = minDis;
                res.push_back(min);
                seen.push_back(min.first);
                notFound = false;
            }
        }
    }
    return res;
}

double ImageProcessor::gaussianFunction(int i, int j, double sigma)
{
    return 1 / (2 * M_PI * (sigma * sigma)) * qExp(-((i * i) + (j * j)) / (2 * (sigma * sigma)));
}

double ImageProcessor::gaussianFunction(int i, double sigma)
{
    return 1 / (qSqrt(2 * M_PI) * sigma) * qExp(-(i * i) / (2 * (sigma * sigma)));
}

double ImageProcessor::sigma(double otherSigma, double resultSigma)
{
    return qSqrt(resultSigma * resultSigma - otherSigma * otherSigma);
}

Image ImageProcessor::descriptorForPoint(QPoint point, const Image &image)
{
    constexpr double step = 2 * M_PI / 8;


    const auto grid {image.subimage(point, 8).toSize(16, 16)};
    auto gradValDir {gradientValDir(grid)};
    const auto coefficient {Kernel::Gaussian(8).toImage().toSize(16, 16)};

    for (auto i {0}; i < gradValDir.first.height(); i++) {
        for (auto j {0}; j < gradValDir.first.width(); j++) {
            gradValDir.first.set(i, j, gradValDir.first.at(i, j) * coefficient.at(i, j));
        }
    }

    Image descriptor {1, 4 * 4 * 8};
    for (auto i {0}; i < grid.height(); i++) {
        for (auto j {0}; j < grid.width(); j++) {
            const auto curDir = gradValDir.second.at(i, j) + M_PI;
            const auto curVal = gradValDir.first.at(i, j);
            const auto b1 {qFloor(curDir / step)};
            const auto b2 {[=]() {
                const auto b2 {curDir > b1 * step + step / 2 ? b1 + 1 : b1 - 1};
                if (b2 == 8) {
                    return 0;
                }
                if (b2 == -1) {
                    return 7;
                }
                return b2;
            }()};
            const auto b1Center {b1 * step + step / 2};
            const auto b2Center {b2 * step + step / 2};
            const auto b1Val {curVal / (b1Center / curDir)};
            const auto b2Val {curVal / (b2Center / curDir)};
            descriptor.add(i, j, b1, b1Val);
            descriptor.add(i, j, b2, b2Val);
        }
    };
    descriptor.normalize();
    descriptor.clamp(0.2);
    descriptor.normalize();
    return descriptor;
}
