#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <QImage>
#include <QPair>
#include <QPoint>

#include "src/structures/image.h"
#include "src/structures/kernel.h"
#include "src/structures/descriptor.h"

using IPoint = QPair<QPoint, double>;

class ImageProcessor
{
public:
    ImageProcessor();

    static Image applyKernel(const Image &image, const Kernel &kernel,
                                   Image::EdgeMethod edgeMethod = Image::EdgeMethod::Reflect);
    static Image applyGaussian(const Image &image, double sigma);
    static Image sobelOperator(const Image &image);
    static QPair<Image, Image> gradientValDir(const Image &image);

    static QVector<IPoint> moravec(const Image &image, int p, int t, int windowSize = 5);
    static QVector<IPoint> harris(const Image &image, int p, int t, int windowSize = 5);
    static QVector<IPoint> anms(const QVector<IPoint> &ipoints, int neededAmount);
    static QVector<IPoint> nms(const Image &image, int p);

    static QVector<QPair<QPoint, Image>> descriptors(const QVector<IPoint> &ipoints, const Image &image);
    static QPair<QPoint, Image> closestDescriptor(const QPair<QPoint, Image> &source, const QVector<QPair<QPoint, Image> > &others);
    static QVector<QPair<QPoint, Image> > closestDescriptor(const QPair<QPoint, Image> &source, QVector<QPair<QPoint, Image> > others, int n);
    static Image descriptorForPoint(QPoint point, const Image &image);

    static double gaussianFunction(int i, int j, double sigma);
    static double gaussianFunction(int i, double sigma);
    static double sigma(double otherSigma, double resultSigma);


private:

};

#endif // IMAGEPROCESSOR_H
