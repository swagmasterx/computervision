#include <QDebug>
#include <QElapsedTimer>
#include <QPainter>

#include "imageprocessor.h"
#include "pyramids.h"

#include "src/cv.common.h"
#include "src/cv.converter.h"
#include "src/structures/cv.vector.h"

enum class LabNumber {
    Lab1,
    Lab2,
    Lab3,
    Lab4,
    Lab5
};

int main(int argc, char *argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv);

    constexpr auto labNumber { LabNumber::Lab4 };

    QImage qimage{":/images/lenna.jpg"};
    QImage qimage2{":/images/Lenna2.jpg"};
    Image image{qimage};
    Image image2{qimage2};

    switch(labNumber) {
    case LabNumber::Lab1: {
        Image result = ImageProcessor::sobelOperator(image);
        result.normalize(0, 255);
        result.toQImage().save("sobel.png");
    } break;
    case LabNumber::Lab2: {
        Pyramids pyramids(image, 3, 5, 0.3, 1.2);
        pyramids.computePyramids();
        pyramids.saveAsImages("pyramids");
        pyramids.outputValues("pyramids");
    } break;
    case LabNumber::Lab3: {
        QVector<IPoint> redDots;
        QImage result;
        QElapsedTimer timer;
        double time;
        timer.start();
        redDots = ImageProcessor::moravec(image, 3, 1000);
        time = static_cast<double>(timer.elapsed())  / 1000;
        qDebug() << "Moravec time: " << time << ", dots: " << redDots.size();
        result = image.toQImage();
        for (const IPoint &dot : redDots) {
            result.setPixelColor(dot.first.x(), dot.first.y(), QColor(255, 0, 0));
        }
        result.save("moravec.png");

        timer.start();
        redDots = ImageProcessor::anms(redDots, 500);
        time = static_cast<double>(timer.elapsed())  / 1000;
        qDebug() << "ANMS Moravec time: " << time << ", dots: " << redDots.size();
        result = image.toQImage();
        for (const IPoint &dot : redDots) {
            result.setPixelColor(dot.first.x(), dot.first.y(), QColor(255, 0, 0));
        }
        result.save("moravec_anms1000.png");

        timer.start();
        redDots = ImageProcessor::harris(image, 3, 1000);
        time = static_cast<double>(timer.elapsed())  / 1000;
        qDebug() << "Harris time: " << time << ", dots: " << redDots.size();
        result = image.toQImage();
        for (const IPoint &dot : redDots) {
            result.setPixelColor(dot.first.x(), dot.first.y(), QColor(255, 0, 0));
        }
        result.save("harris.png");

        timer.start();
        redDots = ImageProcessor::anms(redDots, 500);
        time = static_cast<double>(timer.elapsed())  / 1000;
        qDebug() << "ANMS Harris time: " << time << ", dots: " << redDots.size();
        result = image.toQImage();
        for (const IPoint &dot : redDots) {
            result.setPixelColor(dot.first.x(), dot.first.y(), QColor(255, 0, 0));
        }
        result.save("harris_anms1000.png");
    } break;
    case LabNumber::Lab4: {
        CV::Vector image1 {CV::Converter::qImageToVector(QImage{":/images/fence1.jpg"})};
        CV::Vector image2 {CV::Converter::qImageToVector(QImage{":/images/fence4.jpg"})};
        auto res = CV::Converter::vectorToQImage(CV::Utils::glueTogether(image1, image2));

        image1.normalize(0.0, 1.0);
        image2.normalize(0.0, 1.0);

        auto ipoints1 {CV::harris(image1, 3, 0.05)};
        CV::anms(ipoints1, 20);
        CV::sift(ipoints1, image1);

        auto ipoints2 {CV::harris(image2, 3, 0.05)};
        CV::anms(ipoints2, 20);
        CV::sift(ipoints2, image2);

        QPainter paintDevice {&res};
        paintDevice.setPen(Qt::red);
        paintDevice.setBrush({Qt::red});
        for (const auto &ipoint : ipoints1) {
            const auto pointIt {CV::mapIPoint(ipoint, ipoints2)};
            paintDevice.drawLine(QPoint{ipoint.coord.x, ipoint.coord.y}, QPoint{(*pointIt).coord.x + image1.size().width(), (*pointIt).coord.y});
            paintDevice.drawEllipse(ipoint.coord.x - 8, ipoint.coord.y - 8, 16, 16);
        }
        for (const auto &ipoint : ipoints2) {
            paintDevice.drawEllipse(ipoint.coord.x - 8 + image1.size().width(), ipoint.coord.y - 8, 16, 16);
        }
        res.save("res.png");

    } break;
    case LabNumber::Lab5: {
        CV::Vector image1 {CV::Converter::qImageToVector(QImage{":/images/lenna.jpg"})};
        CV::Vector image2 {CV::Converter::qImageToVector(QImage{":/images/Lenna2.jpg"})};
        auto res = CV::Converter::vectorToQImage(CV::Utils::glueTogether(image1, image2));

        image1.normalize(0.0, 1.0);
        image2.normalize(0.0, 1.0);

        auto ipoints1 {CV::harris(image1, 3, 0.05)};
        CV::anms(ipoints1, 50);
        CV::siftInvRotation(ipoints1, image1);

        auto ipoints2 {CV::harris(image2, 3, 0.05)};
        CV::anms(ipoints2, 50);
        CV::siftInvRotation(ipoints2, image2);

        QPainter paintDevice {&res};
        paintDevice.setPen(Qt::red);
        paintDevice.setBrush({Qt::red});
        for (const auto &ipoint : ipoints1) {
            const auto pointIt {CV::mapIPoint(ipoint, ipoints2)};
            paintDevice.drawLine(QPoint{ipoint.coord.x, ipoint.coord.y}, QPoint{(*pointIt).coord.x + image1.size().width(), (*pointIt).coord.y});
            paintDevice.drawEllipse(ipoint.coord.x - 8, ipoint.coord.y - 8, 16, 16);
        }
        for (const auto &ipoint : ipoints2) {
            paintDevice.drawEllipse(ipoint.coord.x - 8 + image1.size().width(), ipoint.coord.y - 8, 16, 16);
        }
        res.save("res.png");
    } break;
    }
    return 0;
}
