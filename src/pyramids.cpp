#include "pyramids.h"

#include <QDir>
#include <QFile>
#include <QTextStream>

#include "src/imageprocessor.h"

Pyramids::Pyramids(const Image &originalImage, int octaves,
                   int levels, double sigmaA, double sigma0)
    : m_originalImage(originalImage), m_octaves(octaves),
      m_levels(levels), m_sigmaA(sigmaA), m_sigma0(sigma0)
{
    Q_ASSERT(m_levels > 0);
    Q_ASSERT(m_octaves > 0);
}

Image Pyramids::originalImage() const
{
    return m_originalImage;
}

void Pyramids::setOriginalImage(const Image &originalImage)
{
    m_originalImage = originalImage;
}

int Pyramids::octaves() const
{
    return m_octaves;
}

void Pyramids::setOctaves(int octaves)
{
    Q_ASSERT(octaves > 0);
    m_octaves = octaves;
}

int Pyramids::levels() const
{
    return m_levels;
}

void Pyramids::setLevels(int levels)
{
    Q_ASSERT(levels > 0);
    m_levels = levels;
}

double Pyramids::sigmaA() const
{
    return m_sigmaA;
}

void Pyramids::setSigmaA(double sigmaA)
{
    m_sigmaA = sigmaA;
}

double Pyramids::sigma0() const
{
    return m_sigma0;
}

void Pyramids::setSigma0(double sigma0)
{
    m_sigma0 = sigma0;
}

void Pyramids::computePyramids()
{
    m_data.clear();
    Image varImage = m_originalImage;
    varImage = ImageProcessor::applyGaussian(varImage, ImageProcessor::sigma(m_sigmaA, m_sigma0));
    m_data.append(varImage);
    double k = qPow(2, 1. / m_levels);
    for (int i = 0; i < m_octaves; i++) {
        for (int j = 0; j < m_levels; j++) {
            double kernelSigma = ImageProcessor::sigma(m_sigma0 * qPow(k, j),
                                                       m_sigma0 * qPow(k, j + 1));
            varImage = ImageProcessor::applyGaussian(varImage, kernelSigma);
            m_data.append(varImage);
        }
        varImage = varImage.downsample();
        m_data.append(varImage);
    }
}

void Pyramids::saveAsImages(const QString &dirPath) const
{
    QDir pyramidsFolder(dirPath);
    if (!pyramidsFolder.exists()) {
        pyramidsFolder.mkpath(".");
    }
    Image varImage;
    for (int i = 0; i < m_octaves; i++) {
        for (int j = 0; j < m_levels + 1; j++) {
            varImage = m_data.at(i * (m_levels + 1) + j);
            varImage.normalize(0, 255);
            if (j == 0) {
                varImage.toQImage().save(dirPath + QDir::separator()
                                         + "downsample" + QString::number(i) + ".png");
            } else {
                varImage.toQImage().save(dirPath + QDir::separator()
                                         + QString::number(i) + "_"
                                         + QString::number(j - 1) + ".png");
            }
        }
    }
    varImage = m_data.at(m_data.size() - 1);
    varImage.normalize(0, 255);
    varImage.toQImage().save(dirPath + QDir::separator()
                                                 + "downsample"
                                                 + QString::number(m_octaves) + ".png");
}

void Pyramids::outputValues(const QString &dirPath) const
{
    QFile pyramidsValues(dirPath + QDir::separator() + "pyramidsValues.txt");
    Q_ASSERT(pyramidsValues.open(QIODevice::ReadWrite));
    QTextStream stream(&pyramidsValues);
    double k = qPow(2, 1. / m_levels);
    for (int i = 0; i < m_octaves; i++) {
        for (int j = 0; j < m_levels; j++) {
            double kernelSigma = ImageProcessor::sigma(m_sigma0 * qPow(k, j),
                                                       m_sigma0 * qPow(k, j + 1));
            double localSigma = m_sigma0 * qPow(k, j + 1);
            double effectiveSigma = qPow(2, i) * localSigma;
            stream << "octave " << i << ", level " << j << ": kernelSigma " << kernelSigma
                   << ", localSigma " << localSigma
                   << ", effectiveSigma " << effectiveSigma << "\n";
        }
    }
    pyramidsValues.close();
}
