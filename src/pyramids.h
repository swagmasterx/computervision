#ifndef PYRAMIDS_H
#define PYRAMIDS_H

#include "src/structures/image.h"

class Pyramids
{
public:
    Pyramids(const Image &originalImage, int octaves,
             int levels, double sigmaA, double sigma0);

    Image originalImage() const;
    void setOriginalImage(const Image &originalImage);

    int octaves() const;
    void setOctaves(int octaves);

    int levels() const;
    void setLevels(int levels);

    double sigmaA() const;
    void setSigmaA(double sigmaA);

    double sigma0() const;
    void setSigma0(double sigma0);

    void computePyramids();
    void saveAsImages(const QString &dirPath) const;
    void outputValues(const QString &dirPath) const;

private:
    QVector<Image> m_data;
    Image m_originalImage;
    int m_octaves;
    int m_levels;
    double m_sigmaA;
    double m_sigma0;
};

#endif // PYRAMIDS_H
