#include "cv.vector.h"

#include <algorithm>
#include <tuple>
#include <exception>
#include <numeric>

CV::Vector::Vector(CV::Size size)
    : m_data{std::vector<double>(size.height() * size.width())},
      m_size{size}
{
}

CV::Size CV::Vector::size() const
{
    return m_size;
}

double CV::Vector::at(CV::Point p, CV::EdgeMethod edgeMethod) const
{
    if (!contains(p)) {
        switch (edgeMethod) {
        case EdgeMethod::Black:
            return 0.;
        case EdgeMethod::CopyEdge:
            return at({
                (p.y >= m_size.height() ? m_size.height() - 1 : (p.y < 0 ? 0 : p.y)),
                (p.x >= m_size.width() ? m_size.width() - 1 : (p.x < 0 ? 0 : p.x))
            });
        case EdgeMethod::Reflect:
            return at({
                (p.y >= m_size.height() ? m_size.height() - (p.y - m_size.height() + 1) : (p.y < 0 ? -p.y : p.y)),
                (p.x >= m_size.width() ? m_size.width() - (p.x - m_size.width() + 1) : (p.x < 0 ? -p.x : p.x))
            });
        }
    } else {
        return m_data[p.y * m_size.width() + p.x];
    }
}

double CV::Vector::directAt(int index) const
{
    return m_data.at(index);
}

void CV::Vector::set(CV::Point p, double value)
{
    if (!contains(p)) throw std::out_of_range{"p is out of bounds"};

    m_data[p.y * m_size.width() + p.x] = value;
}

void CV::Vector::directSet(int index, double value)
{
    if (index < 0 || index >= m_data.size()) throw std::out_of_range{"index is out of range"};
    m_data[index] = value;
}

bool CV::Vector::contains(CV::Point p) const
{
    return !(p.y >= m_size.height() || p.y < 0 || p.x >= m_size.width() || p.x < 0);
}

CV::Vector CV::Vector::neighborhood(CV::Point center, int radius) const
{
    if (!contains(center)) throw std::out_of_range{"p is out of bounds"};
    if (radius < 0) throw std::invalid_argument{"radius must be non-negative"};

    Vector res {{(radius * 2) + 1, (radius * 2) + 1}};
    for (auto u {-radius}; u <= radius; u++) {
        for (auto v {-radius}; v <= radius; v++) {
            res.set({radius + u, radius + v}, at({center.y + u, center.x + v}));
        }
    }
    return res;
}

CV::Vector &CV::Vector::resize(CV::Size newSize)
{
    Vector res{newSize};
    const auto xStep {static_cast<double>(m_size.width()) / res.m_size.width()};
    const auto yStep {static_cast<double>(m_size.height()) / res.m_size.height()};
    for (auto k {0}; k < res.m_size.height(); k++) {
        for (auto l {0}; l < res.m_size.width(); l++) {
            const auto pi {k * yStep};
            const auto pj {l * xStep};
            const auto i {static_cast<int>(std::floor(pi))};
            const auto j {static_cast<int>(std::floor(pj))};
            const auto fxy1 {((j+1) - pj) * at({i, j}) + (pj - j) * at({i, j + 1})};
            const auto fxy2 {((j+1) - pj) * at({i + 1, j}) + (pj - j) * at({i + 1, j + 1})};
            res.set({k, l}, ((i + 1) - pi) * fxy1 + (pi - i) * fxy2);
        }
    }
    return *this = res;
}

CV::Vector &CV::Vector::normalize()
{
    const auto sum = std::sqrt(std::accumulate(m_data.cbegin(), m_data.cend(), 0., [](auto &sum, const auto &next){
        return sum += next * next;
    }));
    for (auto &val : m_data) {
        val /= sum;
    }
    return *this;
}

CV::Vector &CV::Vector::normalize(double newMin, double newMax)
{
    const auto mm { std::minmax_element( std::cbegin(m_data), std::cend(m_data) ) };

    const auto min { *mm.first };
    const auto max { *mm.second };

    for (auto &val : m_data) {
        val = (max - min) == 0 ? (newMax - newMin) : (val - min) * (newMax - newMin) / (max - min);
    }
}

CV::Vector &CV::Vector::clamp(double threshhold)
{
    for (auto &val : m_data) {
        if (val > threshhold) {
            val = threshhold;
        }
    }
    return *this;
}

CV::Vector &CV::Vector::operator+=(const CV::Vector &other)
{
    if (m_size != other.m_size) throw std::invalid_argument{"vectors must be of equal size"};

    std::transform(
        m_data.begin(), m_data.end(),
        other.m_data.begin(),
        m_data.begin(),
        [](const auto &val1, const auto &val2){return val1 + val2;}
    );
    return *this;
}

CV::Vector &CV::Vector::operator-=(const CV::Vector &other)
{
    if (m_size != other.m_size) throw std::invalid_argument{"vectors must be of equal size"};

    std::transform(
        m_data.begin(), m_data.end(),
        other.m_data.begin(),
        m_data.begin(),
        [](const auto &val1, const auto &val2){return val1 - val2;}
    );
    return *this;
}

CV::Vector &CV::Vector::operator*=(const CV::Vector &other)
{
    if (m_size.width() != other.m_size.height()) throw std::invalid_argument{"v1.width must be equal to v2.height"};

    Vector result({other.m_size.width(), m_size.height()});
    for (auto i {0}; i < result.size().height(); i++) {
        for (auto j {0}; j < result.size().width(); j++) {
            auto sum = 0.;
            for (int k = 0; k < other.size().width(); k++) {
                sum += at({i, k}) * other.at({k, j});
            }
            result.set({i, j}, sum);
        }
    }
    return *this = result;
}

CV::Vector &CV::Vector::operator+=(double val)
{
    std::transform(
        m_data.begin(), m_data.end(),
        m_data.begin(),
        [val](const auto &elem){return elem + val;}
    );
    return *this;
}

CV::Vector &CV::Vector::operator-=(double val)
{
    std::transform(
        m_data.begin(), m_data.end(),
        m_data.begin(),
        [val](const auto &elem){return elem - val;}
    );
    return *this;
}

CV::Vector &CV::Vector::operator*=(double val)
{
    std::transform(
        m_data.begin(), m_data.end(),
        m_data.begin(),
        [val](const auto &elem){return elem * val;}
    );
    return *this;
}

CV::Vector &CV::Vector::operator/=(double val)
{
    std::transform(
        m_data.begin(), m_data.end(),
        m_data.begin(),
        [val](const auto &elem){return elem / val;}
    );
    return *this;
}

double CV::d1(const CV::Vector &x, const CV::Vector &y)
{
    if (x.m_size != y.m_size) throw std::invalid_argument{"vectors must be of equal size"};

    auto sum {0.};
    for (auto i {0}; i < x.m_size.height() * x.m_size.width(); i++) {
        sum += std::pow(x.m_data[i] - y.m_data[i], 2);
    }
    return std::sqrt(sum);
}

CV::Vector CV::operator*(CV::Vector v1, const CV::Vector &v2)
{
    v1 *= v2;
    return v1;
}

CV::Vector CV::operator-(CV::Vector v1, const CV::Vector &v2)
{
    v1 -= v2;
    return v1;
}

CV::Vector CV::operator+(CV::Vector v1, const CV::Vector &v2)
{
    v1 += v2;
    return v1;
}

CV::Vector CV::operator+(CV::Vector v1, double val)
{
    v1 += val;
    return v1;
}

CV::Vector CV::operator-(CV::Vector v1, double val)
{
    v1 -= val;
    return v1;
}

CV::Vector CV::operator*(CV::Vector v1, double val)
{
    v1 *= val;
    return v1;
}

CV::Vector CV::operator/(CV::Vector v1, double val)
{
    v1 /= val;
    return v1;
}

CV::Vector CV::atan2(const CV::Vector &x, const CV::Vector &y)
{
    if (x.m_size != y.m_size) throw std::invalid_argument{"vectors must be of equal size"};

    Vector res{x.m_size};
    std::transform(
        x.m_data.begin(), x.m_data.end(),
        y.m_data.begin(),
        res.m_data.begin(),
        [](const auto &val1, const auto &val2){return std::atan2(val1, val2);}
    );
    return res;
}

CV::Vector CV::pow(const CV::Vector &x, double y)
{
    Vector res{x.m_size};
    std::transform(
        x.m_data.begin(), x.m_data.end(),
        res.m_data.begin(),
        [y](const auto &x) {return std::pow(x, y);}
    );
    return res;
}

bool CV::operator!=(const CV::Vector &v1, const CV::Vector &v2)
{
    return !(v1 == v2);
}

bool CV::operator==(const CV::Vector &v1, const CV::Vector &v2)
{
    return v1.m_data == v2.m_data;
}

CV::Size::Size(int height, int width)
    : m_height{height}, m_width{width}
{
    if (height < 0) throw std::invalid_argument{"height must be >= 0"};
    if (width < 0) throw std::invalid_argument{"width must be >= 0"};
}

int CV::Size::height() const
{
    return m_height;
}

int CV::Size::width() const
{
    return m_width;
}

bool CV::operator==(const CV::Size &s1, const CV::Size &s2)
{
    return std::tie(s1.m_height, s1.m_width) == std::tie(s2.m_height, s2.m_width);
}

bool CV::operator!=(const CV::Size &s1, const CV::Size &s2)
{
    return !(s1 == s2);
}

bool CV::operator==(const CV::Point &p1, const CV::Point &p2)
{
    return std::tie(p1.y, p1.x) == std::tie(p2.y, p2.x);
}

bool CV::operator!=(const CV::Point &p1, const CV::Point &p2)
{
    return !(p1 == p2);
}

double CV::d1(const CV::Point &x, const CV::Point &y)
{
    return std::sqrt(std::pow(x.x - y.x, 2) + std::pow(x.y - y.y, 2));
}
