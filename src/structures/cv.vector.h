#ifndef CV_VECTOR_H
#define CV_VECTOR_H

#include <vector>

namespace CV {

enum class EdgeMethod {
    Black,
    CopyEdge,
    Reflect
};

struct Point {
    friend bool operator==(const Point &p1, const Point &p2);
    friend bool operator!=(const Point &p1, const Point &p2);

    friend double d1(const Point &x, const Point &y);

    int y {0};
    int x {0};
};

class Size {
public:
    Size() = default;
    Size(const Size &) = default;
    Size &operator=(const Size &) = default;
    Size(Size &&) = default;
    Size &operator=(Size &&) = default;
    ~Size() = default;

    Size(int height, int width);

    int height() const;
    int width() const;

    friend bool operator==(const Size &s1, const Size &s2);
    friend bool operator!=(const Size &s1, const Size &s2);

private:
    int m_height {0};
    int m_width {0};
};

class Vector
{
public:
    Vector() = default;
    Vector(const Vector &) = default;
    Vector &operator=(const Vector &) = default;
    Vector(Vector &&) = default;
    Vector &operator=(Vector &&) = default;
    ~Vector() = default;

    explicit Vector(Size size);

    Size size() const;

    double at(Point p, CV::EdgeMethod edgeMethod = CV::EdgeMethod::Reflect) const;
    double directAt(int index) const;
    void set(Point p, double value);
    void directSet(int index, double value);

    bool contains(Point p) const;

    Vector neighborhood(Point center, int radius) const;
    Vector &resize(Size newSize);
    Vector &normalize();
    Vector &normalize(double newMin, double newMax);
    Vector &clamp(double threshhold);

    Vector &operator+=(const Vector &other);
    Vector &operator-=(const Vector &other);
    Vector &operator*=(const Vector &other);

    Vector &operator+=(double val);
    Vector &operator-=(double val);
    Vector &operator*=(double val);
    Vector &operator/=(double val);

    friend bool operator==(const Vector &v1, const Vector &v2);
    friend bool operator!=(const Vector &v1, const Vector &v2);

    friend Vector operator+(Vector v1, const Vector &v2);
    friend Vector operator-(Vector v1, const Vector &v2);
    friend Vector operator*(Vector v1, const Vector &v2);

    friend Vector operator+(Vector v1, double val);
    friend Vector operator-(Vector v1, double val);
    friend Vector operator*(Vector v1, double val);
    friend Vector operator/(Vector v1, double val);

    friend Vector pow(const Vector &x, double y);
    friend Vector atan2(const Vector &x, const Vector &y);
    friend double d1(const Vector &x, const Vector &y);

private:
    std::vector<double> m_data;
    Size m_size;
};

bool operator==(const Size &s1, const Size &s2);
bool operator!=(const Size &s1, const Size &s2);

bool operator==(const Point &p1, const Point &p2);
bool operator!=(const Point &p1, const Point &p2);

bool operator==(const Vector &v1, const Vector &v2);
bool operator!=(const Vector &v1, const Vector &v2);

Vector operator+(Vector v1, const Vector &v2);
Vector operator-(Vector v1, const Vector &v2);
Vector operator*(Vector v1, const Vector &v2);

Vector operator+(Vector v1, double val);
Vector operator-(Vector v1, double val);
Vector operator*(Vector v1, double val);
Vector operator/(Vector v1, double val);

Vector pow(const Vector &x, double y);
Vector atan2(const Vector &x, const Vector &y);
double d1(const Vector &x, const Vector &y);

}
#endif // CV_VECTOR_H
