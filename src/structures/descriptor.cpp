#include "descriptor.h"

Descriptor::Descriptor(int width, int height, int bins)
    : m_width{width}, m_height{height}, m_bins{bins}
{
    m_histograms.fill({1, m_bins}, m_width * m_height);
}

int Descriptor::width() const
{
    return m_width;
}

int Descriptor::height() const
{
    return  m_height;
}



void Descriptor::set(int i, int j, int bin, double val)
{
    Q_ASSERT(i >= 0 && i < m_height);
    Q_ASSERT(j >= 0 && j < m_width);
    Q_ASSERT(bin >= 0 && bin < m_bins);
}
