#ifndef DESCRIPTOR_H
#define DESCRIPTOR_H

#include "src/structures/image.h"

class Descriptor
{
public:
    Descriptor() = default;
    Descriptor(const Descriptor &) = default;
    Descriptor(Descriptor &&) = default;
    Descriptor &operator=(const Descriptor &) = default;
    Descriptor &operator=(Descriptor &&) = default;
    ~Descriptor() = default;

    Descriptor(int width, int height, int bins);

    int width() const;
    int height() const;

    void set(int i, int j, int bin, double val);

private:
    QVector<Image> m_histograms;
    int m_width {0};
    int m_height {0};
    int m_bins {0};
};

#endif // DESCRIPTOR_H
