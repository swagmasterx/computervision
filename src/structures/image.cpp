#include "image.h"

#include <QImage>
#include <QtMath>
#include <utility>

Image::Image(int width, int height) : m_width(width), m_height(height)
{
    Q_ASSERT(width > 0);
    Q_ASSERT(height > 0);
    m_data.fill(0, m_width * m_height);
}

Image::Image(const QImage &image) : m_width(image.width()), m_height(image.height())
{
    Q_ASSERT(!image.isNull());
    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width; j++) {
            m_data.append(qColortoGreyscale(image.pixelColor(j, i)));
        }
    }
}

int Image::width() const
{
    return m_width;
}

int Image::height() const
{
    return m_height;
}

double Image::at(int i, int j, EdgeMethod edgeMethod) const
{
    auto value {0.};
    if (i >= m_height || i < 0 || j >= m_width || j < 0) {
        switch (edgeMethod) {
        case EdgeMethod::Black:
            // it's already defaulted to 0
            break;
        case EdgeMethod::CopyEdge:
            value = at((i >= m_height ?
                                m_height - 1 : (i < 0 ? 0 : i)),
                           (j >= m_width ?
                                m_width - 1 : (j < 0 ? 0 : j)));
            break;
        case EdgeMethod::Reflect:
            value = at((i >= m_height ? m_height - (i - m_height + 1)
                                                            : (i < 0 ? -i : i)),
                                       (j >= m_width ? m_width - (j - m_width + 1)
                                                           : (j < 0 ? -j : j)));
            break;
        }
    } else {
        value = m_data.at(i * m_width + j);
    }
    return value;
}

void Image::set(int i, int j, double value)
{
    Q_ASSERT_X(j >= 0 && j < m_width, "set", QString::number(j).toUtf8().constData());
    Q_ASSERT_X(i >= 0 && i < m_height, "set", QString::number(i).toUtf8().constData());
    m_data[i * m_width + j] = value;
}

void Image::add(int i, int j, int bin, double val)
{
    const auto bi {i - i % 4};
    const auto bj {j - j % 4};

    m_data[bi * 4 + bj + bin] += val;
}

QImage Image::toQImage() const
{
    QImage qimage(m_width, m_height, QImage::Format_RGB32);
    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width; j++) {
            int greyscale = m_data[i * m_width + j];
            qimage.setPixelColor(j, i, QColor(greyscale, greyscale, greyscale));
        }
    }
    return qimage;
}

Image Image::downsample() const
{
    Q_ASSERT(m_width > 1);
    Q_ASSERT(m_height > 1);
    Image newImage(m_width % 2 ? m_width / 2 + 1 : m_width / 2,
                   m_height % 2 ? m_height / 2 + 1 : m_height / 2);
    for (int i = 0; i < newImage.m_height; i++) {
        for (int j = 0; j < newImage.m_width; j++) {
            newImage.m_data[i * newImage.m_width + j] = m_data[i * 2 * m_width + j * 2];
        }
    }
    return newImage;
}

Image Image::subimage(QPoint pos, int radius) const
{
    Q_ASSERT(radius > 0);
    Q_ASSERT(pos.x() >= 0);
    Q_ASSERT(pos.y() >= 0);
    Image res {(radius * 2) + 1, (radius * 2) + 1};
    for (auto u {-radius}; u <= radius; u++) {
        for (auto v {-radius}; v <= radius; v++) {
            res.set(radius + u, radius + v, at(pos.y() + u, pos.x() + v));
        }
    }
    return res;
}

Image Image::toSize(int width, int height) const
{
    Image res{width, height};
    const auto xStep {static_cast<double>(m_width) / res.m_width};
    const auto yStep {static_cast<double>(m_height) / res.m_height};
    for (auto k {0}; k < res.m_height; k++) {
        for (auto l {0}; l < res.m_width; l++) {
            const auto pi {k * yStep};
            const auto pj {l * xStep};
            const auto i {qFloor(pi)};
            const auto j {qFloor(pj)};
            const auto fxy1 {((j+1) - pj) * at(i, j) + (pj - j) * at(i, j + 1)};
            const auto fxy2 {((j+1) - pj) * at(i + 1, j) + (pj - j) * at(i + 1, j + 1)};
            res.set(k, l, ((i + 1) - pi) * fxy1 + (pi - i) * fxy2);
        }
    }
    return res;
}

Image &Image::operator+=(const Image &other)
{
    Q_ASSERT(m_width == other.m_width);
    Q_ASSERT(m_height == other.m_height);
    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width; j++) {
            m_data[i * m_width + j] += other.m_data.at(i * other.m_width + j);
        }
    }
    return *this;
}

double distance(const Image &x, const Image &y)
{
    Q_ASSERT(x.m_width == y.m_width);
    Q_ASSERT(x.m_height == y.m_height);
    Image result{x.m_width, x.m_height};
    auto sum {0.};
    for (auto i {0}; i < x.m_height * x.m_width; i++) {
        sum += qPow(x.m_data[i] - y.m_data[i], 2);
    }
    return qSqrt(sum);
}

Image atan2(const Image &x, const Image &y)
{
    Q_ASSERT(x.m_width == y.m_width);
    Q_ASSERT(x.m_height == y.m_height);
    Image result(x.m_width, x.m_height);
    for (int i = 0; i < result.m_height; i++) {
        for (int j = 0; j < result.m_width; j++) {
            result.m_data[i * result.m_width + j] = atan2(x.m_data[i * x.m_width + j],
                                 y.m_data[i * y.m_width + j]);
        }
    }
    return result;
}

Image pow(const Image &x, double y)
{
    Image result(x.m_width, x.m_height);
    for (int i = 0; i < result.m_height; i++) {
        for (int j = 0; j < result.m_width; j++) {
            result.m_data[i * result.m_width + j] = pow(x.m_data.at(i * x.m_width + j), y);
        }
    }
    return result;
}

Image operator+(Image i1, const Image &i2)
{
    i1 += i2;
    return i1;
}

void Image::normalize(double newMin, double newMax)
{
    const auto mm { std::minmax_element( std::cbegin(m_data), std::cend(m_data) ) };

    const auto min { *mm.first };
    const auto max { *mm.second };

    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width; j++) {
            double it = m_data.at(i * m_width + j);
            m_data[i * m_width + j] = (max - min) == 0 ?
                        (newMax - newMin) : (it - min) * (newMax - newMin) / (max - min);
        }
    }
}

void Image::normalize()
{
    const auto sum = std::sqrt(std::accumulate(m_data.constBegin(), m_data.constEnd(), 0., [](auto sum, auto next){
        return sum += next * next;
    }));
    for (auto &val : m_data) {
        val /= sum;
    }
}

void Image::clamp(double threshhold)
{
    for (auto &val : m_data) {
        if (val > threshhold) {
            val = threshhold;
        }
    }
}

double Image::qColortoGreyscale(const QColor &qcolor)
{
    return 0.2126 * qcolor.red() + 0.7152 * qcolor.green() + 0.0722 * qcolor.blue();
}
