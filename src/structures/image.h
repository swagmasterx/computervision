#ifndef IMAGE_H
#define IMAGE_H

#include <QVector>
#include <QColor>
#include <QImage>
#include <QtMath>
#include <QPoint>

class Image
{
public:
    enum class EdgeMethod {
        Black,
        CopyEdge,
        Reflect
    };

    Image() = default;
    Image(const Image &) = default;
    Image &operator=(const Image &) = default;
    Image(Image &&image) = default;
    Image &operator=(Image &&other) = default;
    ~Image() = default;

    Image(int width, int height);
    Image(const QImage &image);

    int width() const;
    int height() const;

    double at(int i, int j, EdgeMethod edgeMethod = EdgeMethod::Reflect) const;
    void set(int i, int j, double value);
    void add(int i, int j, int bin, double val);

    QImage toQImage() const;
    Image downsample() const;

    Image subimage(QPoint pos, int radius) const;
    Image toSize(int width, int height) const;

    Image &operator+=(const Image &other);

    friend Image operator+(Image i1, const Image &i2);
    friend Image pow(const Image &x, double y);
    friend Image atan2(const Image &x, const Image &y);
    friend double distance(const Image &x, const Image &y);

    void normalize(double newMin, double newMax);
    void normalize();
    void clamp(double threshhold);

    static double qColortoGreyscale(const QColor &qcolor);

private:
    QVector<double> m_data;
    int m_width {0};
    int m_height {0};
};

#endif // IMAGE_H
