#include "kernel.h"

#include <QtMath>
#include <QDebug>
#include <utility>
#include "src/imageprocessor.h"

Kernel::Kernel(int width, int height) : m_width(width), m_height(height)
{
    Q_ASSERT(m_width % 2 == 1);
    Q_ASSERT(m_height % 2 == 1);
    m_data.fill(0, m_width * m_height);
}

Kernel::Kernel(std::initializer_list<std::initializer_list<double>> args) :
    m_width((*args.begin()).size()), m_height(args.size())
{
    Q_ASSERT(m_width % 2 == 1);
    Q_ASSERT(m_height % 2 == 1);
    for (auto i = args.begin(); i != args.end(); i++) {
        Q_ASSERT((*i).size() == m_width);
        for (auto j = (*i).begin(); j != (*i).end(); j++ ) {
            m_data.append(*j);
        }
    }
}

double Kernel::at(int i, int j) const
{
    Q_ASSERT_X(j >= 0 && j < m_width, "at", QString::number(j).toUtf8().constData());
    Q_ASSERT_X(i >= 0 && i < m_height, "at", QString::number(i).toUtf8().constData());
    return m_data.at(i * m_width + j);
}

void Kernel::set(int i, int j, double value)
{
    Q_ASSERT_X(j >= 0 && j < m_width, "set", QString::number(j).toUtf8().constData());
    Q_ASSERT_X(i >= 0 && i < m_height, "set", QString::number(i).toUtf8().constData());
    m_data[i * m_width + j] = value;
}

int Kernel::width() const
{
    return m_width;
}

int Kernel::height() const
{
    return m_height;
}

int Kernel::centerX() const
{
    return m_width / 2;
}

int Kernel::centerY() const
{
    return m_height / 2;
}

Image Kernel::toImage() const
{
    Image res{m_width, m_height};
    for (auto i {0}; i < m_height; i++) {
        for (auto j {0}; j < m_width; j++) {
            res.set(i, j, at(i, j));
        }
    }
    return res;
}

Kernel &Kernel::operator+=(const Kernel &other)
{
    Q_ASSERT(m_width == other.m_width);
    Q_ASSERT(m_height == other.m_height);
    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width; j++) {
             m_data[i * m_width + j] += other.m_data[i * m_width + j];
        }
    }
    return *this;
}

Kernel &Kernel::operator*=(double multiplier)
{
    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width; j++) {
            m_data[i * m_width * j] *= multiplier;
        }
    }
    return *this;
}

Kernel Kernel::derativeXX()
{
    return Kernel({{1, 0, -1}});
}

Kernel Kernel::derativeXY()
{
    return Kernel({
                      {1},
                      {2},
                      {1}
                  });
}

Kernel Kernel::derativeYX()
{
    return Kernel({{1, 2, 1}});
}

Kernel Kernel::derativeYY()
{
    return Kernel({
                      {1},
                      {0},
                      {-1}
                  });
}

Kernel Kernel::GaussianX(double sigma)
{
    Kernel result(((int)(6 * sigma) % 2) ? (int)(6 * sigma) : ((int)(6 * sigma) + 1), 1);
    for (int j = 0; j < result.m_width; j++) {
        result.m_data[j] = ImageProcessor::gaussianFunction(j - result.centerX(), sigma);
    }
    return result;
}

Kernel Kernel::GaussianY(double sigma)
{
    Kernel result(1, ((int)(6 * sigma) % 2) ? (int)(6 * sigma) : ((int)(6 * sigma) + 1));
    for (int i = 0; i < result.m_height; i++) {
        result.m_data[i] = ImageProcessor::gaussianFunction(i - result.centerY(), sigma);
    }
    return result;
}

Kernel Kernel::Gaussian(int halfSize)
{
    Kernel result(halfSize * 2 + 1, halfSize * 2 + 1);
    double sigma = (double) halfSize / 3;
    for (int i = 0; i < result.m_height; i++) {
        for (int j = 0; j < result.m_width; j++) {
            result.m_data[i * result.m_width + j] = ImageProcessor::gaussianFunction
                    (i - result.centerY(), j - result.centerX(), sigma);
        }
    }
    return result;
}

Kernel operator+(Kernel k1, const Kernel &k2)
{
    k1 += k2;
    return k1;
}

Kernel operator*(Kernel k, double multiplier)
{
    k *= multiplier;
    return k;
}

Kernel operator*(const Kernel &k1, const Kernel &k2)
{
    Q_ASSERT(k1.m_width == k2.m_height);
    Kernel result(k2.m_width, k1.m_height);
    for (int i = 0; i < k1.m_height; i++) {
        for (int j = 0; j < k2.m_width; j++) {
            double sum = 0;
            for (int k = 0; k < k1.m_width; k++) {
                sum += k1.m_data[i * k1.m_width + k] * k2.m_data[k * k2.m_width + j];
            }
            result.m_data[i * result.m_width + j] = sum;
        }
    }
    return result;
}
