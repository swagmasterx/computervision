#ifndef CORE_H
#define CORE_H

#include <QVector>
#include "src/structures/image.h"

class Kernel
{
public:
    Kernel() = default;
    Kernel(const Kernel &) = default;
    Kernel &operator=(const Kernel &) = default;
    Kernel(Kernel &&) = default;
    Kernel &operator=(Kernel &&) = default;
    ~Kernel() = default;

    Kernel(int width, int height);
    Kernel(std::initializer_list<std::initializer_list<double>> args);

    double at(int i, int j) const;
    void set(int i, int j, double value);

    int width() const;
    int height() const;

    int centerX() const;
    int centerY() const;

    Image toImage() const;

    Kernel &operator+=(const Kernel &other);
    Kernel &operator*=(double multiplier);

    friend Kernel operator*(const Kernel &k1, const Kernel &k2);
    friend Kernel operator*(Kernel k, double multiplier);
    friend Kernel operator+(Kernel k1, const Kernel &c2);

    static Kernel derativeXX();
    static Kernel derativeXY();

    static Kernel derativeYX();
    static Kernel derativeYY();

    static Kernel GaussianX(double sigma);
    static Kernel GaussianY(double sigma);
    static Kernel Gaussian(int halfSize);

private:
     QVector<double> m_data;
     int m_width;
     int m_height;
};

#endif // CORE_H
